﻿using OnlineLibrary.Domain.Entities;
using OnlineLibrary.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Data.Entity;

namespace OnlineLibrary.Data.EntityFramework.Repositories
{
    internal class UserRepository : Repository<User>, IUserRepository
    {
        internal UserRepository(ApplicationDbContext context)
            : base (context)
        { }

        public User FindByUserName(string userName)
        {
            return Set.FirstOrDefault(x => x.UserName == userName);
        }

        public Task<User> FindByUserNameAsync(string userName)
        {
            return Set.FirstOrDefaultAsync(x => x.UserName == userName);
        }

        public Task<User> FindByUserNameAsync(CancellationToken cancellationToken, string userName)
        {
            return Set.FirstOrDefaultAsync(x => x.UserName == userName, cancellationToken);
        }
    }
}
