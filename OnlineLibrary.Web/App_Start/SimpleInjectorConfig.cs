﻿using Microsoft.AspNet.Identity;
using OnlineLibrary.Data.EntityFramework;
using OnlineLibrary.Domain;
using OnlineLibrary.Web.Identity;
using SimpleInjector;
using SimpleInjector.Advanced;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineLibrary.Web
{
    public static class SimpleInjectorConfig
    {
        public static void RegisterComponents()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            container.Register<IUnitOfWork>(() => new UnitOfWork("OnlineLibrary"), Lifestyle.Singleton);
            container.Register<IUserStore<IdentityUser, Guid>, UserStore>(Lifestyle.Singleton);
            container.Register<RoleStore>(Lifestyle.Singleton);

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}